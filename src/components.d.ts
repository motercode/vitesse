// generated by unplugin-vue-components
// We suggest you to commit this file into source control
// Read more: https://github.com/vuejs/vue-next/pull/3399
import '@vue/runtime-core'

declare module '@vue/runtime-core' {
  export interface GlobalComponents {
    CAlert: typeof import('./components/chakraExamples/CAlert.vue')['default']
    ChakraTest: typeof import('./components/ChakraTest.vue')['default']
    CInput: typeof import('./components/chakraExamples/CInput.vue')['default']
    ComponentsNavigation: typeof import('./components/ComponentsNavigation.vue')['default']
    Counter: typeof import('./components/Counter.vue')['default']
    ErrorComponent: typeof import('./components/ErrorComponent.vue')['default']
    Footer: typeof import('./components/Footer.vue')['default']
    JSComponent: typeof import('./components/chakraExamples/JSComponent.vue')['default']
    LoadingComponent: typeof import('./components/LoadingComponent.vue')['default']
    Navbar: typeof import('./components/Navbar.vue')['default']
    PiniaTest: typeof import('./components/PiniaTest.vue')['default']
    README: typeof import('./components/README.md')['default']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
    SelectExample: typeof import('./components/chakraExamples/SelectExample.vue')['default']
    TSComponent: typeof import('./components/chakraExamples/TSComponent.vue')['default']
  }
}

export {}
