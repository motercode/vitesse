# i360crawlerTestFramework

un test del framework que usaremos para el proyecto , tambien testear el ci

## inciar el proyecto

- `npm install` instala lo necesario no da errores.
- `npm run test` se tiran los test unitarios en continuo para el develops.
- `npm run dev`  sirve en develop por el puerto 3333 del localhost y abre una ventana en el navegador
- `npm run lint` se pasa el eslint
- para hacer test e2e hay que hacer primero el build y servirlo por el 4171 de esta forma:

```
    npm run build:noSSG
    npm run preview
    npm test:e2e 
```
## archivios adjuntos no propios del proyecto

Un archivo netlify.toml que sirve     para facilitar el deploy en netlify ahora para hacer comprobaciones hasta que se empiece el proyecto en si . 

Directorio .vscode donde vienen las extensiones que uso yo , no es necesario cargarlas.

## Ejemplo uso de pinia
*TODO*
```
export const useStore = defineStore('main', {
  state: () => ({
  }),
  getters: {
    
  },
  actions: {
  }
}
```

## Ejemplo uso Chakra
*TODO*

## como esta montado el proyecto

- la base ha sido vitesse que es un template de vite para vue. 
    [link al github del proyecto](https://github.com/antfu/vitesse)
```
    npx degit antfu/vitesse vitesseapp
    rm -R vitesseapp/.github
    npm install -g pnpm
    pnpm install 
```
en este paso puede aparecer el error 
```
     ERR_PNPM_PEER_DEP_ISSUES  Unmet peer dependencies
    .
    └─┬ @intlify/vite-plugin-vue-i18n
    └── ✕ unmet peer vue-i18n@next: found 9.1.10
```
Esta ya metido como deuda técnica , no afecta en el proyecto por ahora.

### configuración del proyecto 

seguimos los pasos de :
GitHub - antfu/vitesse: 🏕 Opinionated Vite Starter Template 
en este paso 

“Change the hostname in vite.config.ts”

no aparece el hostname lo mas parecido son las claves de server.host o preview.host, lo hemos pasado a deuda técnica pues no parece que afecte al proyecto.

“… remove routes”

no se a lo que se refiere con esto , porque se crean según los módulos que vas poniendo , creo un issue para mirarlo bien.

se puede seguir con el proyecto .

SSG workarround

creamos el script  build:noSSG  
o bien solo para pinia descomentamos las lineas que hay en el main.ts

### repasando todos los scripts de pnpm

se han creado task y bugs de lo que no se entiende "preview-https": "serve dist", por ejemplo.
añadimos los scripts : 

- ```"dev:silent": "vite --port 3333"```

que sirve para poder poner el servidor en marcha sin que te abra una tab del navegador.

- ```"test:e2e:silent": "cypress run"```

lanza los test de cypress sin abrir el inteface , mostrando el resultado en el terminal.

- ```"lint:ci": "eslint . --format junit -o lint/lintReport.xml"``` 
para generar el informe en lint/lintReport.xml 

- ```"test:unit": "vitest --run"``` 

lo modificamos pues hay dos iguales que llamaban a vitest, en este caso test:unit hace un solo test sin activar el watch

- ```"test:and:coverage": "vitest run --coverage"```

```pnpm i c8```

la usaremos para producir el informe del test de cobertura compatible con gitlab. 

y añadimos esto al final de la configuración de vite.config.ts
```
 coverage: {
      reporter: ['text', 'cobertura'],
    },
``` 

```"test:e2e:silent": "cypress run",
    "test:e2e:silent:chrome": "cypress run --browser chrome "
```
para lanzar los tes en CI de forma automatica.

```
"test:and:coverage": "vitest run --coverage --reporter junit --outputFile unitTestReports/junitStyleReport.xml"
```
para lanzar los test unitarios en CI y producir el reporte necesario tanto de covertura como de test unitarios.

```
"build:noSSG": "vite build"
```
añadimos lo siguiente para hacer los build sin SSG de forma que sorteamos los problemas con modulos commonJS 

Configuramos eslint para ignore patterns cypress segun indica en el index.js comentados

```
dist
public
cypress/plugins/index.js  
cache/*
cypress/support/index.js
y en .eslintrc
```
y configuramos el eslintrc
```
{
  "env": {
    "browser": true,
    "es2021": true
  },
  "extends": "eslint:recommended",
  "parserOptions": {
    "ecmaVersion": "latest"
  },
  "rules": {
  },
  "globals": {
    "i4t": true,
    "pdfjsLib": true,
    "module": true
  }
}
```

### añadir chakra al proyecto
```
pnpm i @chakra-ui/vue-next
pnpm i -D @babel/core
```
create file on modules chakra.ts  
```
import ChakraUIVuePlugin, { chakra } from "@chakra-ui/vue-next";
import { domElements } from "@chakra-ui/vue-system";
import { type UserModule } from '~/types'

export const install: UserModule = ({ app, router, isClient }) => {
    app.use(ChakraUIVuePlugin)
  
  
  domElements.forEach((tag) => {
    app.component(`chakra.${tag}`, chakra(tag));
  });
}
```

### Configuracion del Gitlab CI

creamos el archivo en el ./ del proyecto llamado .gitlab-ci.yml
Stages:
- prepare: hace la instalacion necesaria de modulos de node y lo guarda en cache para ser usado en el resto.
-test: lanza los test de eslint y unitarios.
-build: crea el dist.
-teste: lanza los test de cypress.
-accesibility: lanza los test de ay11 sobre las paginas que pongamos en la variable.

 
 

### configurar los mensajes de gitlab

seguir las pautas del link para configurar los mensajes cada usuario 

Notification emails | GitLab 

https://gitlab.com/-/profile/notifications

usar custom notifications activar las de pipeline

